/*
 * Copyright (C) 2017 thtf, Inc. All Rights Reserved.
 */
package com.thtf.ocr.ui.util;

import android.content.res.Resources;

public class DimensionUtil {

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
